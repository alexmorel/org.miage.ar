package org.miage.ar.android.internal.application.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

/**
 * Created by alexmorel on 16/10/2017.
 */

public class GsonManager {

    public static Gson INSTANCE = initializeGson();

    private static Gson initializeGson() {
        return new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls().create();
    }
}
