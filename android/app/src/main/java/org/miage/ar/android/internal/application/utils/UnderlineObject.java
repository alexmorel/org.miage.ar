package org.miage.ar.android.internal.application.utils;

import java.nio.Buffer;

/**
 * Created by alexmorel on 16/10/2017.
 */

public class UnderlineObject  extends org.miage.ar.android.internal.application.utils.MeshObject
{

    private static final double bigSize = 1;
    private static final double littleSize = 0.05;
    private static final double textCoordsValue = 1;
    private static final double cubeNormalsValue = 1;

    // Data for drawing the 3D plane as overlay
    private static final double cubeVertices[]  = {
            -bigSize, -littleSize, littleSize, // front
            bigSize, -littleSize, littleSize,
            bigSize, littleSize, littleSize,
            -bigSize, littleSize, littleSize,

            -bigSize, -littleSize, -littleSize, // back
            bigSize, -littleSize, -littleSize,
            bigSize, littleSize, -littleSize,
            -bigSize, littleSize, -littleSize,

            -bigSize, -littleSize, -littleSize, // left
            -bigSize, -littleSize, littleSize,
            -bigSize, littleSize, littleSize,
            -bigSize, littleSize, -littleSize,

            bigSize, -littleSize, -littleSize, // right
            bigSize, -littleSize, littleSize,
            bigSize, littleSize, littleSize,
            bigSize, littleSize, -littleSize,

            -bigSize, littleSize, littleSize, // top
            bigSize, littleSize, littleSize,
            bigSize, littleSize, -littleSize,
            -bigSize, littleSize, -littleSize,

            -bigSize, -littleSize, littleSize, // bottom
            bigSize, -littleSize, littleSize,
            bigSize, -littleSize, -littleSize,
            -bigSize, -littleSize, -littleSize };


    private static final double cubeTexcoords[] = {
            0, 0, textCoordsValue, 0, textCoordsValue, textCoordsValue, 0, textCoordsValue,

            textCoordsValue, 0, 0, 0, 0, textCoordsValue, textCoordsValue, textCoordsValue,

            0, 0, textCoordsValue, 0, textCoordsValue, textCoordsValue, 0, textCoordsValue,

            textCoordsValue, 0, 0, 0, 0, textCoordsValue, textCoordsValue, textCoordsValue,

            0, 0, textCoordsValue, 0, textCoordsValue, textCoordsValue, 0, textCoordsValue,

            textCoordsValue, 0, 0, 0, 0, textCoordsValue, textCoordsValue, textCoordsValue  };


    private static final double cubeNormals[]   = {
            0, 0, cubeNormalsValue,  0, 0, cubeNormalsValue,  0, 0, cubeNormalsValue,  0, 0, cubeNormalsValue,

            0, 0, -cubeNormalsValue, 0, 0, -cubeNormalsValue, 0, 0, -cubeNormalsValue, 0, 0, -cubeNormalsValue,

            -cubeNormalsValue, 0, 0, -cubeNormalsValue, 0, 0, -cubeNormalsValue, 0, 0, -cubeNormalsValue, 0, 0,

            cubeNormalsValue, 0, 0,  cubeNormalsValue, 0, 0,  cubeNormalsValue, 0, 0,  cubeNormalsValue, 0, 0,

            0, cubeNormalsValue, 0,  0, cubeNormalsValue, 0,  0, cubeNormalsValue, 0,  0, cubeNormalsValue, 0,

            0, -cubeNormalsValue, 0, 0, -cubeNormalsValue, 0, 0, -cubeNormalsValue, 0, 0, -cubeNormalsValue, 0,
    };

    private static final short  cubeIndices[]   = {
            0, 1, 2, 0, 2, 3, // front
            4, 6, 5, 4, 7, 6, // back
            8, 9, 10, 8, 10, 11, // left
            12, 14, 13, 12, 15, 14, // right
            16, 17, 18, 16, 18, 19, // top
            20, 22, 21, 20, 23, 22  // bottom
    };

    private Buffer mVertBuff;
    private Buffer mTexCoordBuff;
    private Buffer mNormBuff;
    private Buffer mIndBuff;


    public UnderlineObject()
    {
        mVertBuff = fillBuffer(cubeVertices);
        mTexCoordBuff = fillBuffer(cubeTexcoords);
        mNormBuff = fillBuffer(cubeNormals);
        mIndBuff = fillBuffer(cubeIndices);
    }


    @Override
    public Buffer getBuffer(BUFFER_TYPE bufferType)
    {
        Buffer result = null;
        switch (bufferType)
        {
            case BUFFER_TYPE_VERTEX:
                result = mVertBuff;
                break;
            case BUFFER_TYPE_TEXTURE_COORD:
                result = mTexCoordBuff;
                break;
            case BUFFER_TYPE_INDICES:
                result = mIndBuff;
                break;
            case BUFFER_TYPE_NORMALS:
                result = mNormBuff;
            default:
                break;
        }
        return result;
    }


    @Override
    public int getNumObjectVertex()
    {
        return cubeVertices.length / 3;
    }


    @Override
    public int getNumObjectIndex()
    {
        return cubeIndices.length;
    }
}
