package org.miage.ar.android.api.model;

/**
 * Created by alexmorel on 16/10/2017.
 */

public class RecognizedWord {
    public String word;
    public double recognitionScore;

    public static String pictoToWord(String pictoName) {
        if (pictoName.equals("33_1203")) {
            return "33/1203";
        } else if (pictoName.equals("336_1230")) {
            return "336/1230";
        }
        return "mot";
    }
}
