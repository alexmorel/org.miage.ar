package org.miage.ar.android.api.model;

/**
 * Created by alexmorel on 10/10/2017.
 */

public class RecognizedPictogram {
    public static final double SCORE_LIVE = 1;
    public static final double SCORE_OPENCV = 2;
    public static final double SCORE_BOTH = 3;
    public int pictoId;
    public String pictoName;
    public double recognitionScore;

    public static String getPictoName(String shapeName) {
        if (shapeName.contains("gaz")) {
            return "Gaz comprimé, liquéfié";
        } else if (shapeName.contains("comburant")) {
            return "Matière ou gaz comburant";
        } else if (shapeName.contains("explosive")) {
            return "Matières et objets explosibles";
        } else {
            if (shapeName.contains("number") || shapeName.contains("word")) {
                return shapeName.replace("number_", "").replace("word_", "");
            }
            if (shapeName.contains("_") && (!(shapeName.contains("_bis")))) {
                return shapeName.split("_")[0];
            } else if (shapeName.equals("skull_bis")) {
                return "skull6";
            }
            return shapeName;
        }
    }

    public static String getPictoDisplayName(String pictoName) {
        if (pictoName.equals("Matières et objets explosibles")) {
            return "Explosive";
        } else if (pictoName.equals("Gaz comprimé, liquéfié")) {
            return "Non-flammable, non-toxic gas";
        } else if (pictoName.equals("Matière ou gaz comburant")) {
            return "Oxidizing agents";
        } else if (pictoName.equals("acid")) {
            return "Corrosive substances";
        } else if (pictoName.equals("aquapollution")) {
            return "Environmentally hazardous substances";
        } else if (pictoName.equals("exclam")) {
            return "Caution";
        } else if (pictoName.equals("inflamable")) {
            return "Flammable gas";
        } else if (pictoName.equals("skull")) {
            return "Poison";
        } else if (pictoName.equals("skull6")) {
            return "Toxic substances";
        }
        return pictoName;
    }
}
