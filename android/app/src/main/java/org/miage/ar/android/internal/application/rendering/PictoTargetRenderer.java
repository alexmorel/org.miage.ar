package org.miage.ar.android.internal.application.rendering;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.DisplayMetrics;
import android.util.Log;

import com.vuforia.Device;
import com.vuforia.Matrix34F;
import com.vuforia.Matrix44F;
import com.vuforia.State;
import com.vuforia.Tool;
import com.vuforia.Trackable;
import com.vuforia.TrackableResult;
import com.vuforia.Vuforia;

import org.miage.ar.android.api.activity.RecognitionActivity;
import org.miage.ar.android.internal.application.AppRenderer;
import org.miage.ar.android.internal.application.AppRendererControl;
import org.miage.ar.android.internal.application.ApplicationSession;
import org.miage.ar.android.internal.application.utils.CubeObject;
import org.miage.ar.android.internal.application.utils.CubeShaders;
import org.miage.ar.android.internal.application.utils.LoadingDialogHandler;
import org.miage.ar.android.internal.application.utils.MeshObject;
import org.miage.ar.android.internal.application.utils.SampleUtils;
import org.miage.ar.android.internal.application.utils.Texture;
import org.miage.ar.android.internal.application.utils.UnderlineObject;

import java.nio.IntBuffer;
import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class PictoTargetRenderer implements GLSurfaceView.Renderer, AppRendererControl
{
    private static final String LOGTAG = "ImageTargetRenderer";

    private ApplicationSession miageAppSession;
    private RecognitionActivity mActivity;
    private AppRenderer mAppRenderer;

    private Vector<Texture> mTextures;

    private int shaderProgramID;
    private int vertexHandle;
    private int textureCoordHandle;
    private int mvpMatrixHandle;
    private int texSampler2DHandle;

    private MeshObject mCube;
    private MeshObject mUnderline;

    private float kBuildingScale = 0.012f;

    private boolean mIsActive = false;
    private boolean mModelIsLoaded = false;

    private static final float OBJECT_SCALE_FLOAT = 0.003f;
    private static final float TEXT_SCALE_FLOAT = 0.005f;
    private static final float TEXT_PADDING_TOP = -0.0025f;
    private long lastScreenshotSentDate;
    private Bitmap lastScreenshot = null;
    private GL10 gl10;
    private boolean mTakeScreenShot;


    public PictoTargetRenderer(RecognitionActivity activity, ApplicationSession session)
    {
        mActivity = activity;
        miageAppSession = session;
        // AppRenderer used to encapsulate the use of RenderingPrimitives setting
        // the device mode AR/VR and stereo mode
        mAppRenderer = new AppRenderer(this, mActivity, Device.MODE.MODE_AR, false, 0.01f , 5f);
    }


    // Called to draw the current frame.
    @Override
    public void onDrawFrame(GL10 gl)
    {
        if (!mIsActive)
            return;
        this.gl10 = gl;
        mAppRenderer.render();
    }


    public void setActive(boolean active)
    {
        mIsActive = active;

        if(mIsActive)
            mAppRenderer.configureVideoBackground();
    }


    // Called when the surface is created or recreated.
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        Log.d(LOGTAG, "GLRenderer.onSurfaceCreated");

        // Call Vuforia function to (re)initialize rendering after first use
        // or after OpenGL ES context was lost (e.g. after onPause/onResume):
        miageAppSession.onSurfaceCreated();

        mAppRenderer.onSurfaceCreated();
    }


    // Called when the surface changed size.
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Log.d(LOGTAG, "GLRenderer.onSurfaceChanged");

        // Call Vuforia function to handle render surface size changes:
        miageAppSession.onSurfaceChanged(width, height);

        // RenderingPrimitives to be updated when some rendering change is done
        mAppRenderer.onConfigurationChanged(mIsActive);

        initRendering();
    }


    // Function for initializing the renderer.
    private void initRendering()
    {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f
                : 1.0f);

        for (Texture t : mTextures)
        {
            GLES20.glGenTextures(1, t.mTextureID, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t.mTextureID[0]);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA,
                    t.mWidth, t.mHeight, 0, GLES20.GL_RGBA,
                    GLES20.GL_UNSIGNED_BYTE, t.mData);
        }

        shaderProgramID = SampleUtils.createProgramFromShaderSrc(
                CubeShaders.CUBE_MESH_VERTEX_SHADER,
                CubeShaders.CUBE_MESH_FRAGMENT_SHADER);

        vertexHandle = GLES20.glGetAttribLocation(shaderProgramID,
                "vertexPosition");
        textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramID,
                "vertexTexCoord");
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID,
                "modelViewProjectionMatrix");
        texSampler2DHandle = GLES20.glGetUniformLocation(shaderProgramID,
                "texSampler2D");

        if(!mModelIsLoaded) {
            mCube = new CubeObject();
            mUnderline = new UnderlineObject();

            mModelIsLoaded = true;

            // Hide the Loading Dialog
            mActivity.loadingDialogHandler
                    .sendEmptyMessage(LoadingDialogHandler.HIDE_LOADING_DIALOG);
        }

    }

    public void updateConfiguration()
    {
        mAppRenderer.onConfigurationChanged(mIsActive);
    }

    public void takeScreenshot(boolean takeScreenShot) {
        mTakeScreenShot = true;
    }
    private void takeScreenshot(GL10 gl) {
        // Take screenshot if needed
        if (mTakeScreenShot && System.currentTimeMillis() - lastScreenshotSentDate > RecognitionActivity.RECOGNITION_DELAY) {
            lastScreenshotSentDate = System.currentTimeMillis();
            mTakeScreenShot = false;

            DisplayMetrics dm = new DisplayMetrics();
            mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
            int w = dm.widthPixels;
            int h = dm.heightPixels;
            int x = 0;
            int y = 0;
            int b[] = new int[w * (y + h)];
            int bt[] = new int[w * h];
            IntBuffer ib = IntBuffer.wrap(b);
            ib.position(0);
            //gl.glReadPixels(x, 0, w, y + h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, ib);
            gl.glCopyTexImage2D(0, 0, GLES20.GL_RGBA, 0, 0, w, h, 0);
            GLES20.glReadPixels(0, 0, w, h, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, ib);
            for (int i = 0, k = 0; i < h; i++, k++) {//remember, that OpenGL bitmap is incompatible with Android bitmap
                //and so, some correction need.
                for (int j = 0; j < w; j++) {
                    int pix = b[i * w + j];
                    int pb = (pix >> 16) & 0xff;
                    int pr = (pix << 16) & 0x00ff0000;
                    int pix1 = (pix & 0xff00ff00) | pr | pb;
                    bt[(h - k - 1) * w + j] = pix1;
                }
            }

            Bitmap screenshot = Bitmap.createBitmap(bt, w, h, Bitmap.Config.ARGB_8888);
            mActivity.screenshotTaken(screenshot);
        }
    }

    // The render function called from SampleAppRendering by using RenderingPrimitives views.
    // The state is owned by AppRenderer which is controlling it's lifecycle.
    // State should not be cached outside this method.
    public void renderFrame(State state, float[] projectionMatrix)
    {
        // Renders video background replacing Renderer.DrawVideoBackground()
        mAppRenderer.renderVideoBackground();
        takeScreenshot(gl10);

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        // handle face culling, we need to detect if we are using reflection
        // to determine the direction of the culling
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glCullFace(GLES20.GL_BACK);

        // Did we find any trackables this frame?
        for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++) {
            TrackableResult result = state.getTrackableResult(tIdx);
            Trackable trackable = result.getTrackable();
            printUserData(trackable);
            Matrix34F pose = result.getPose();
            //Tool.setTranslation(pose, new Vec3F(translationX, translationY, translationZ));
            //Tool.setRotation(pose, new Vec3F(translationX, translationY, translationZ), rotationAngle);
            Matrix44F modelViewMatrix_Vuforia = Tool
                    .convertPose2GLMatrix(pose);
            float[] modelViewMatrix = modelViewMatrix_Vuforia.getData();

            int textureIndex = getTextureIndex(trackable);
            mActivity.didRecognizedTrackableShape(trackable, lastScreenshot);

            // deal with the modelview and projection matrices
            float[] modelViewProjection = new float[16];

            if (!mActivity.isExtendedTrackingActive()) {
                applyModelTransformations(trackable, modelViewMatrix);
            } else {
                Matrix.rotateM(modelViewMatrix, 0, 90.0f, 1.0f, 0, 0);
                Matrix.scaleM(modelViewMatrix, 0, kBuildingScale,
                        kBuildingScale, kBuildingScale);
            }
            Matrix.multiplyMM(modelViewProjection, 0, projectionMatrix, 0, modelViewMatrix, 0);

            // activate the shader program and bind the vertex/normal/tex coords
            GLES20.glUseProgram(shaderProgramID);

            MeshObject meshObject = mCube;
            if (isText(trackable.getName())) {
                //meshObject = mUnderline;
            }
            if (!mActivity.isExtendedTrackingActive()) {
                GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT,
                        false, 0, meshObject.getVertices());
                GLES20.glVertexAttribPointer(textureCoordHandle, 2,
                        GLES20.GL_FLOAT, false, 0, meshObject.getTexCoords());

                GLES20.glEnableVertexAttribArray(vertexHandle);
                GLES20.glEnableVertexAttribArray(textureCoordHandle);

                // activate texture 0, bind it, and pass to shader
                GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                        mTextures.get(textureIndex).mTextureID[0]);
                GLES20.glUniform1i(texSampler2DHandle, 0);

                // pass the model view matrix to the shader
                GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false,
                        modelViewProjection, 0);

                // finally draw the teapot
                GLES20.glDrawElements(GLES20.GL_TRIANGLES,
                        meshObject.getNumObjectIndex(), GLES20.GL_UNSIGNED_SHORT,
                        meshObject.getIndices());

                // disable the enabled arrays
                GLES20.glDisableVertexAttribArray(vertexHandle);
                GLES20.glDisableVertexAttribArray(textureCoordHandle);
            }
        }
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);

    }

    private boolean isText(String name) {
        return name.contains("number");
    }

    private int getTextureIndex(Trackable trackable) {
        String trackableName = trackable.getName().toLowerCase();
        if (trackableName.contains("acid")) {
            return 0;
        } else if (trackableName.contains("aquapollution")) {
            return 1;
        } else if (trackableName.contains("comburant")) {
            return 2;
        } else if (trackableName.contains("exclam")) {
            return 3;
        } else if (trackableName.contains("explosive")) {
            return 4;
        } else if (trackableName.contains("gaz")) {
            return 5;
        } else if (trackableName.contains("inflamable")) {
            return 6;
        } else if (trackableName.contains("skull_bis")) {
            return 8;
        } else if (trackableName.contains("skull")) {
            return 7;
        } else if (trackableName.contains("number_336")) {
            return 10;
        }
        return 9;
    }

    private void applyModelTransformations(Trackable trackable, float[] modelViewMatrix) {
        if (trackable.getName().contains("number")) {
            Matrix.translateM(modelViewMatrix, 0, 0.0f, TEXT_PADDING_TOP,  0.0f);
            Matrix.scaleM(modelViewMatrix, 0, TEXT_SCALE_FLOAT,
                   TEXT_SCALE_FLOAT, TEXT_SCALE_FLOAT);
        } else {
            Matrix.rotateM(modelViewMatrix, 0, 45f, 0, 0, 1f);
            Matrix.scaleM(modelViewMatrix, 0, OBJECT_SCALE_FLOAT,
                    OBJECT_SCALE_FLOAT, OBJECT_SCALE_FLOAT);
        }
    }

    private void printUserData(Trackable trackable)
    {
        String userData = (String) trackable.getUserData();
        Log.d(LOGTAG, "UserData:Retreived User Data	\"" + userData + "\"");
    }


    public void setTextures(Vector<Texture> textures)
    {
        mTextures = textures;

    }

}
