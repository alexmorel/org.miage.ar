package org.miage.ar.android.internal.events;

import org.miage.ar.android.api.model.RecognizedPictogram;

/**
 * Created by alexmorel on 10/10/2017.
 * Event launched when a pictogram is recognized.
 */
public class PictoRecognizedEvent {
    private RecognizedPictogram recognizedPictogram;

    public PictoRecognizedEvent(RecognizedPictogram recognizedPictogram) {
        this.recognizedPictogram = recognizedPictogram;
    }

    /**
     * Returns the recognized pictogram (including its recognition score).
     * @return the recognized pictogram
     */
    public RecognizedPictogram getRecognizedPictogram() {
        return recognizedPictogram;
    }
}
