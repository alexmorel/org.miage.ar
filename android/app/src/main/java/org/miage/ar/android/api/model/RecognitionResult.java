package org.miage.ar.android.api.model;

import org.miage.ar.android.internal.application.model.GsonManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexmorel on 16/10/2017.
 */

public class RecognitionResult {

    public List<RecognizedPictogram> recognizedPictograms = new ArrayList<>();
    public List<RecognizedWord> recognizedWords = new ArrayList<>();

    public String toString() {
        return GsonManager.INSTANCE.toJson(this);
    }

    public static RecognitionResult fromString(String s){
        return GsonManager.INSTANCE.fromJson(s, RecognitionResult.class);
    }

}
