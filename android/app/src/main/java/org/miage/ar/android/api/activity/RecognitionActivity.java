/**
 * Created by alexmorel on 09/10/2017.
 */
package org.miage.ar.android.api.activity;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.irealite.miage.android.VuforiaSamples.R;
import com.squareup.otto.Subscribe;
import com.vuforia.CameraDevice;
import com.vuforia.DataSet;
import com.vuforia.ObjectTracker;
import com.vuforia.STORAGE_TYPE;
import com.vuforia.State;
import com.vuforia.Trackable;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;
import com.vuforia.Vuforia;

import org.miage.ar.android.api.model.RecognitionResult;
import org.miage.ar.android.api.model.RecognizedPictogram;
import org.miage.ar.android.api.model.RecognizedWord;
import org.miage.ar.android.internal.application.ApplicationControl;
import org.miage.ar.android.internal.application.ApplicationException;
import org.miage.ar.android.internal.application.ApplicationSession;
import org.miage.ar.android.internal.application.rendering.PictoTargetRenderer;
import org.miage.ar.android.internal.application.utils.ApplicationGLView;
import org.miage.ar.android.internal.application.utils.LoadingDialogHandler;
import org.miage.ar.android.internal.application.utils.Texture;
import org.miage.ar.android.internal.events.EventBusManager;
import org.miage.ar.android.internal.events.PictoRecognizedEvent;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Vector;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Activity in charge of recognizing pictograms and text.
 */
public class RecognitionActivity  extends Activity implements ApplicationControl {
    private static final String LOGTAG = "RecognitionActivity";
    // Minimal delay between two calls to screenshot
    public static int RECOGNITION_DELAY = 2000;
    // If true, then screenshot will be triggered by vuforia detection
    private static boolean VUFORIA_TRIGGERED_SCREENSHOT = false;
    // If true, then screenshot will be triggered by vuforia detection
    private static boolean PERIODIC_SCREENSHOT = false;
    // If both values are false, screenshot will have to be triggered manually (by touching the surface or the button)

    private static long lastRecognitionTime;

    public static int maxHeight = 1024;
    public static int referenceDensity = 320;

    ApplicationSession miageAppSession;

    private DataSet mCurrentDataset;
    private int mCurrentDatasetSelectionIndex = 0;
    private int mStartDatasetsIndex = 0;
    private int mDatasetsNumber = 0;
    private ArrayList<String> mDatasetStrings = new ArrayList<String>();

    // Our OpenGL view:
    private ApplicationGLView mGlView;

    // Our renderer:
    private PictoTargetRenderer mRenderer;

    private GestureDetector mGestureDetector;

    // The textures we will use for rendering:
    private Vector<Texture> mTextures;

    private boolean mSwitchDatasetAsap = false;
    private boolean mFlash = false;
    private boolean mContAutofocus = true;
    private boolean mExtendedTracking = false;

    private View mFocusOptionView;
    private View mFlashOptionView;

    private RelativeLayout mUILayout;

    public LoadingDialogHandler loadingDialogHandler = new LoadingDialogHandler(this);

    // Alert Dialog used to display SDK errors
    private AlertDialog mErrorDialog;

    boolean mIsDroidDevice = false;
    private Set<Trackable> recognizedTrackables = new LinkedHashSet<>();


    // Detail pannel fields
    private RelativeLayout detailPanel;
    private TextView detailPanelText;
    private HashMap<Integer, RecognizedPictogram> recognizedPictograms = new LinkedHashMap();
    private RecognitionResult recognitionResult = new RecognitionResult();
    private int screenshotWidth;
    private int screenshotHeight;
    private long lastScreenshotSentDate;
    private GestureListener mGestureListener;


    // Called when the activity first starts or the user navigates back to an
    // activity.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOGTAG, "onCreate");
        super.onCreate(savedInstanceState);
        boolean fontFileExists = false;
        String fontPath = "fonts/Roboto-Regular.ttf";
        AssetManager mg = getResources().getAssets();
        InputStream is = null;
        try {
            is = mg.open(fontPath);
            fontFileExists = true;
            //File exists so do something with it
        } catch (IOException ex) {
            //file does not exist
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    // silent
                }
            }
        }
        if (fontFileExists) {
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath(fontPath)
                    .build()
            );
        }
        // Step 1: plug dataset containing image targets to recognize
        miageAppSession = new ApplicationSession(this);
        startLoadingAnimation();
        mDatasetStrings.add("Novyspec.xml");

        // Step 2: init AR
        miageAppSession
                .initAR(this, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mGestureListener = new GestureListener();
        mGestureDetector = new GestureDetector(this, mGestureListener);
        mTextures = new Vector<Texture>();
        loadTextures();
        mIsDroidDevice = android.os.Build.MODEL.toLowerCase().startsWith(
                "droid");
    }

    /**
     * Called whenever an image target (vuforia) gets recognized.
     * @param recognizedTrackableShape
     */
    public void didRecognizedTrackableShape(Trackable recognizedTrackableShape, Bitmap screenshot) {
        // If Picto recognizer call is triggered by vuforia recognition
        if (VUFORIA_TRIGGERED_SCREENSHOT) {
            // Check last recognition time (to avoid spamming the picto recognizer)
            if (System.currentTimeMillis() - lastRecognitionTime > RECOGNITION_DELAY) {
                lastRecognitionTime = System.currentTimeMillis();
                // Delegate actual recognition to the PictoRecognizer
                // If any pictogram actually recognized, a PictoRecognizedEvent will be raised and the didRecognizedPictogram method will be called.
                String shapeName = recognizedTrackableShape.getName();
                if (shapeName.contains("_")) {
                    shapeName = shapeName.split("_")[0];
                }
                sendCameraScreenShot(shapeName, screenshot);
            }
        }
        RecognizedPictogram recognizedPicto = new RecognizedPictogram();
        recognizedPicto.pictoId = recognizedTrackableShape.getName().hashCode();
        recognizedPicto.pictoName = RecognizedPictogram.getPictoName(recognizedTrackableShape.getName().toLowerCase());
        recognizedPicto.recognitionScore = RecognizedPictogram.SCORE_LIVE;
        EventBusManager.BUS.post(new PictoRecognizedEvent(recognizedPicto));
    }

    /**
     * Called whenever the PictoRecognizer actually recognized an event
     */
    @Subscribe
    public void didRecognizedPictogram(final PictoRecognizedEvent recognizedEvent) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (recognizedEvent.getRecognizedPictogram() != null) {
                    boolean lastRecognitionWasWord = false;
                    if (recognizedPictograms.containsKey(recognizedEvent.getRecognizedPictogram().pictoId)) {
                        RecognizedPictogram recognizedPictogram = recognizedPictograms.get(recognizedEvent.getRecognizedPictogram().pictoId);
                        if (recognizedPictogram.recognitionScore == RecognizedPictogram.SCORE_LIVE && recognizedEvent.getRecognizedPictogram().recognitionScore == RecognizedPictogram.SCORE_OPENCV) {
                            recognizedPictogram.recognitionScore = RecognizedPictogram.SCORE_BOTH;
                        } else if (recognizedPictogram.recognitionScore == RecognizedPictogram.SCORE_LIVE && recognizedEvent.getRecognizedPictogram().recognitionScore == RecognizedPictogram.SCORE_OPENCV) {
                            recognizedPictogram.recognitionScore = RecognizedPictogram.SCORE_BOTH;
                        }
                        recognizedPictograms.put(recognizedPictogram.pictoId, recognizedPictogram);
                    } else {
                        recognizedPictograms.put(recognizedEvent.getRecognizedPictogram().pictoId, recognizedEvent.getRecognizedPictogram());
                        if (recognizedEvent.getRecognizedPictogram().pictoName.contains("3") || recognizedEvent.getRecognizedPictogram().pictoName.contains("number") || recognizedEvent.getRecognizedPictogram().pictoName.contains("text")) {
                            RecognizedWord word = new RecognizedWord();
                            word.word = RecognizedWord.pictoToWord(recognizedEvent.getRecognizedPictogram().pictoName);
                            word.recognitionScore = recognizedEvent.getRecognizedPictogram().recognitionScore;
                            recognitionResult.recognizedWords.add(0, word);
                            lastRecognitionWasWord = true;
                        } else {
                            recognitionResult.recognizedPictograms.add(0, recognizedEvent.getRecognizedPictogram());
                        }
                    }
                    String text = "";
                    String pictoText = "";
                    String wordText = "";
                    if (!recognitionResult.recognizedPictograms.isEmpty()) {
                        pictoText = RecognitionActivity.this.getString(R.string.recognized_pictos);
                        for (RecognizedPictogram pictogram : recognitionResult.recognizedPictograms) {
                            pictoText = pictoText + "\n" + "- " + RecognizedPictogram.getPictoDisplayName(pictogram.pictoName);
                            if (pictogram.recognitionScore == RecognizedPictogram.SCORE_LIVE) {
                                pictoText = pictoText + " (1/3)";
                            } else if (pictogram.recognitionScore == RecognizedPictogram.SCORE_OPENCV) {
                                pictoText = pictoText + " (2/3)";
                            } else {
                                pictoText = pictoText + " (3/3)";
                            }
                        }
                    }
                    if (!recognitionResult.recognizedWords.isEmpty()) {
                        wordText = wordText + "\n" + RecognitionActivity.this.getString(R.string.recognized_words);
                        for (RecognizedWord word : recognitionResult.recognizedWords) {
                            wordText = wordText + "\n" + "- " + word.word;
                            if (word.recognitionScore == RecognizedPictogram.SCORE_LIVE) {
                                wordText = wordText + " (1/3)";
                            } else if (word.recognitionScore == RecognizedPictogram.SCORE_OPENCV) {
                                wordText = wordText + " (2/3)";
                            }  else {
                                wordText = wordText + " (3/3)";
                            }
                        }
                    }
                    if (lastRecognitionWasWord) {
                        text = wordText + pictoText;
                    } else {
                        text = pictoText + wordText;
                    }
                    if (text.isEmpty()) {
                        text = RecognitionActivity.this.getString(R.string.searching_picto);
                    }
                    text += "\n";
                    detailPanelText.setText(text);
                }
            }
        });

    }

    /**
     * Saves the surface view as bitmap.
     */
    private void sendCameraScreenShot(String shapeName, Bitmap screenshot)    {
        if (screenshot != null) {
            // can use screenshot here to do computation
        }
    }

    /**
     * Called each time vuforia renderers are called (screenshot must be taken at this time)
     */
    public void screenshotTaken(Bitmap screenshot) {
        // Launch take screenshot if recognition is not vuroria based
        if (!PERIODIC_SCREENSHOT) {
            sendCameraScreenShot("", screenshot);
        }
    }

    public int getScreenshotWidth() {
        return screenshotWidth;
    }

    public int getScreenshotHeight() {
        return screenshotHeight;
    }

    // Process Single Tap event to trigger autofocus
    private class GestureListener extends
            GestureDetector.SimpleOnGestureListener {
        // Used to set autofocus one second after a manual focus is triggered
        private final Handler autofocusHandler = new Handler();


        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }


        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            boolean result = CameraDevice.getInstance().setFocusMode(
                    CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO);
            if (!result)
                Log.e("SingleTapUp", "Unable to trigger focus");

            // Generates a Handler to trigger continuous auto-focus
            // after 1 second
            autofocusHandler.postDelayed(new Runnable() {
                public void run() {
                    mRenderer.takeScreenshot(true);
                    if (mContAutofocus) {
                        final boolean autofocusResult = CameraDevice.getInstance().setFocusMode(
                                CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);
                    }
                }
            }, 1000L);

            return true;
        }
    }


    // We want to load specific textures from the APK, which we will later use
    // for rendering.

    private void loadTextures() {
        mTextures.add(Texture.loadTextureFromApk("Textures/texture_acid.png",
                getAssets()));
        mTextures.add(Texture.loadTextureFromApk("Textures/texture_aquapollution.png",
                getAssets()));
        mTextures.add(Texture.loadTextureFromApk("Textures/texture_comburant.png",
                getAssets()));
        mTextures.add(Texture.loadTextureFromApk("Textures/texture_exclam.png",
                getAssets()));
        mTextures.add(Texture.loadTextureFromApk("Textures/texture_explosive.png",
                getAssets()));
        mTextures.add(Texture.loadTextureFromApk("Textures/texture_gaz.png",
                getAssets()));
        mTextures.add(Texture.loadTextureFromApk("Textures/texture_inflamable.png",
                getAssets()));
        mTextures.add(Texture.loadTextureFromApk("Textures/texture_skull.png",
                getAssets()));
        mTextures.add(Texture.loadTextureFromApk("Textures/texture_skull_bis.png",
                getAssets()));
        mTextures.add(Texture.loadTextureFromApk("Textures/number_33_1203.png",
                getAssets()));
        mTextures.add(Texture.loadTextureFromApk("Textures/number_336_1230.png",
                getAssets()));
    }


    // Called when the activity will start interacting with the user.
    @Override
    public void onResume() {
        Log.d(LOGTAG, "onResume");
        super.onResume();

        showProgressIndicator(true);

        // This is needed for some Droid devices to force portrait
        if (mIsDroidDevice) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        miageAppSession.onResume();

        EventBusManager.BUS.register(this);
    }


    // Callback for configuration changes the activity handles itself
    @Override
    public void onConfigurationChanged(Configuration config) {
        Log.d(LOGTAG, "onConfigurationChanged");
        super.onConfigurationChanged(config);

        miageAppSession.onConfigurationChanged();
    }


    // Called when the system is about to start resuming a previous activity.
    @Override
    public void onPause() {
        EventBusManager.BUS.unregister(this);

        Log.d(LOGTAG, "onPause");
        super.onPause();

        if (mGlView != null) {
            mGlView.setVisibility(View.INVISIBLE);
            mGlView.onPause();
        }

        try {
            miageAppSession.pauseAR();
        } catch (ApplicationException e) {
            Log.e(LOGTAG, e.getString());
        }
    }


    // The final call you receive before your activity is destroyed.
    @Override
    protected void onDestroy() {
        Log.d(LOGTAG, "onDestroy");
        super.onDestroy();

        try {
            miageAppSession.stopAR();
        } catch (ApplicationException e) {
            Log.e(LOGTAG, e.getString());
        }

        // Unload texture:
        mTextures.clear();
        mTextures = null;

        System.gc();
    }

    // Initializes AR application components.
    private void initApplicationAR() {

        // Create OpenGL ES view:
        int depthSize = 2;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();

        mGlView = new ApplicationGLView(this);
        mGlView.init(translucent, depthSize, stencilSize);

        mRenderer = new PictoTargetRenderer(this, miageAppSession);
        mRenderer.setTextures(mTextures);
        mGlView.setRenderer(mRenderer);
    }


    private void startLoadingAnimation() {
        mUILayout = (RelativeLayout) View.inflate(this, R.layout.camera_overlay,
                null);

        mUILayout.setVisibility(View.VISIBLE);
        mUILayout.setBackgroundColor(Color.BLACK);

        // Gets a reference to the loading dialog
        loadingDialogHandler.mLoadingDialogContainer = mUILayout
                .findViewById(R.id.loading_indicator);

        // Shows the loading indicator at start
        loadingDialogHandler
                .sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);

        // Adds the inflated layout to the view
        addContentView(mUILayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

    }


    // Methods to load and destroy tracking data.
    @Override
    public boolean doLoadTrackersData() {
        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset == null)
            mCurrentDataset = objectTracker.createDataSet();

        if (mCurrentDataset == null)
            return false;

        if (!mCurrentDataset.load(
                mDatasetStrings.get(mCurrentDatasetSelectionIndex),
                STORAGE_TYPE.STORAGE_APPRESOURCE))
            return false;

        if (!objectTracker.activateDataSet(mCurrentDataset))
            return false;

        int numTrackables = mCurrentDataset.getNumTrackables();
        for (int count = 0; count < numTrackables; count++) {
            Trackable trackable = mCurrentDataset.getTrackable(count);
            if (isExtendedTrackingActive()) {
                trackable.startExtendedTracking();
            }

            String name = "Current Dataset : " + trackable.getName();
            trackable.setUserData(name);
            Log.d(LOGTAG, "UserData:Set the following user data "
                    + (String) trackable.getUserData());
        }

        return true;
    }


    @Override
    public boolean doUnloadTrackersData() {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset != null && mCurrentDataset.isActive()) {
            if (objectTracker.getActiveDataSet(0).equals(mCurrentDataset)
                    && !objectTracker.deactivateDataSet(mCurrentDataset)) {
                result = false;
            } else if (!objectTracker.destroyDataSet(mCurrentDataset)) {
                result = false;
            }

            mCurrentDataset = null;
        }

        return result;
    }

    @Override
    public void onVuforiaResumed() {
        if (mGlView != null) {
            mGlView.setVisibility(View.VISIBLE);
            mGlView.onResume();
        }
    }

    @Override
    public void onVuforiaStarted() {
        mRenderer.updateConfiguration();

        if (mContAutofocus) {
            // Set camera focus mode
            if (!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO)) {
                // If continuous autofocus mode fails, attempt to set to a different mode
                if (!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO)) {
                    CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_NORMAL);
                }
            }
        }
        if (detailPanel == null) {
            detailPanel = (RelativeLayout) View.inflate(RecognitionActivity.this, R.layout.detail_panel,
                    null);
            detailPanelText = (TextView)detailPanel.findViewById(R.id.detail_panel_text);
            detailPanelText.setText(RecognitionActivity.this.getString(R.string.searching_picto));
            ((Button)detailPanel.findViewById(R.id.detail_panel_button_opencv)).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    mGestureListener.onSingleTapUp(null);
                }
            });
            ((Button)detailPanel.findViewById(R.id.detail_panel_button_ok)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("recognitionResult", recognitionResult.toString());
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }
            });
            ((Button)detailPanel.findViewById(R.id.detail_panel_button_cancel)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("recognitionResult", new RecognitionResult().toString());
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }
            });

            // Adds the inflated layout to the view
            addContentView(detailPanel, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
        }
        showProgressIndicator(false);
    }


    public void showProgressIndicator(boolean show) {
        if (loadingDialogHandler != null) {
            if (show) {
                loadingDialogHandler
                        .sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);
            } else {
                loadingDialogHandler
                        .sendEmptyMessage(LoadingDialogHandler.HIDE_LOADING_DIALOG);
            }
        }
    }


    @Override
    public void onInitARDone(ApplicationException exception) {

        if (exception == null) {
            initApplicationAR();

            mRenderer.setActive(true);

            // Now add the GL surface view. It is important
            // that the OpenGL ES surface view gets added
            // BEFORE the camera is started and video
            // background is configured.
            addContentView(mGlView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));

            // Sets the UILayout to be drawn in front of the camera
            mUILayout.bringToFront();

            // Sets the layout background to transparent
            mUILayout.setBackgroundColor(Color.TRANSPARENT);

            miageAppSession.startAR(CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_DEFAULT);

        } else {
            Log.e(LOGTAG, exception.getString());
            showInitializationErrorMessage(exception.getString());
        }
    }


    // Shows initialization error messages as System dialogs
    public void showInitializationErrorMessage(String message) {
        final String errorMessage = message;
        runOnUiThread(new Runnable() {
            public void run() {
                if (mErrorDialog != null) {
                    mErrorDialog.dismiss();
                }

                // Generates an Alert Dialog to show the error message
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        RecognitionActivity.this);
                builder
                        .setMessage(errorMessage)
                        .setTitle(getString(R.string.INIT_ERROR))
                        .setCancelable(false)
                        .setIcon(0)
                        .setPositiveButton(getString(R.string.button_OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });

                mErrorDialog = builder.create();
                mErrorDialog.show();
            }
        });
    }


    @Override
    public void onVuforiaUpdate(State state) {
        if (mSwitchDatasetAsap) {
            mSwitchDatasetAsap = false;
            TrackerManager tm = TrackerManager.getInstance();
            ObjectTracker ot = (ObjectTracker) tm.getTracker(ObjectTracker
                    .getClassType());
            if (ot == null || mCurrentDataset == null
                    || ot.getActiveDataSet(0) == null) {
                Log.d(LOGTAG, "Failed to swap datasets");
                return;
            }

            doUnloadTrackersData();
            doLoadTrackersData();
        }
    }


    @Override
    public boolean doInitTrackers() {
        // Indicate if the trackers were initialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker;

        // Trying to initialize the image tracker
        tracker = tManager.initTracker(ObjectTracker.getClassType());
        if (tracker == null) {
            Log.e(
                    LOGTAG,
                    "Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else {
            Log.i(LOGTAG, "Tracker successfully initialized");
        }
        return result;
    }


    @Override
    public boolean doStartTrackers() {
        // Indicate if the trackers were started correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.start();

        return result;
    }


    @Override
    public boolean doStopTrackers() {
        // Indicate if the trackers were stopped correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.stop();

        return result;
    }


    @Override
    public boolean doDeinitTrackers() {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ObjectTracker.getClassType());

        return result;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }


    public boolean isExtendedTrackingActive() {
        return mExtendedTracking;
    }

    final public static int CMD_BACK = -1;
    final public static int CMD_EXTENDED_TRACKING = 1;
    final public static int CMD_AUTOFOCUS = 2;
    final public static int CMD_FLASH = 3;
    final public static int CMD_CAMERA_FRONT = 4;
    final public static int CMD_CAMERA_REAR = 5;
    final public static int CMD_DATASET_START_INDEX = 6;

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
