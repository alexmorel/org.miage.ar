package org.miage.ar.android.internal.events;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by alexmorel on 10/10/2016.
 */
public final class EventBusManager {

    public static Bus BUS = new Bus(ThreadEnforcer.ANY);
}
