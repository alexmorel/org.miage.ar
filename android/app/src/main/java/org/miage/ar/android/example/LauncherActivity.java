/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

package org.miage.ar.android.example;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.irealite.miage.android.VuforiaSamples.R;

import org.miage.ar.android.api.activity.RecognitionActivity;
import org.miage.ar.android.api.activity.TextRecognitionActivity;
import org.miage.ar.android.api.model.RecognitionResult;
import org.miage.ar.android.api.model.RecognizedPictogram;
import org.miage.ar.android.api.model.RecognizedWord;


/**
 * Example activity to show how picto recognition activity can be launched and interrogated to get
 * the recognized pictograms.
 */
public class LauncherActivity extends Activity
{

    private static final int RECOGNITION_REQUEST = 1042;
    private static long SPLASH_MILLIS = 450;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        LayoutInflater inflater = LayoutInflater.from(this);
        RelativeLayout layout = (RelativeLayout) inflater.inflate(
                R.layout.reco_result, null, false);

        addContentView(layout, new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
    }

    public void launchTextReco(View v) {
        startActivity(new Intent(this, TextRecognitionActivity.class));
    }

    public void launchPictoReco(View v) {
        Intent pictoRecognitionIntent = new Intent(this, RecognitionActivity.class);
        startActivityForResult(pictoRecognitionIntent, RECOGNITION_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOGNITION_REQUEST) {
            if (resultCode == RESULT_OK) {
                // Step 1: get recognized pictograms
               RecognitionResult recognitionResult = RecognitionResult.fromString(data.getStringExtra("recognitionResult"));

               // Step 2: use result
                String text = "Derniers résultats : \n";
                if (!recognitionResult.recognizedPictograms.isEmpty()) {
                    text = "Pictogrammes :\n";
                    for (RecognizedPictogram pictogram : recognitionResult.recognizedPictograms) {
                        text = text + "\n" + "-" + pictogram.pictoName;
                    }
                }
                if (!recognitionResult.recognizedWords.isEmpty()) {
                    text = text + "\n Mots";
                    for (RecognizedWord word : recognitionResult.recognizedWords) {
                        text = text + "\n" + "-" + word.word;
                    }
                }
                ((TextView)findViewById(R.id.reco_result_text)).setText(text);

            }
        }
    }
}

