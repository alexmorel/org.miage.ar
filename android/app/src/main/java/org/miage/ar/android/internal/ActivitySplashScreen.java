/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package org.miage.ar.android.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.irealite.miage.android.VuforiaSamples.R;

import org.miage.ar.android.api.activity.RecognitionActivity;
import org.miage.ar.android.api.activity.TextRecognitionActivity;
import org.miage.ar.android.example.LauncherActivity;


public class ActivitySplashScreen extends Activity
{
    
    private static long SPLASH_MILLIS = 450;
    
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        LayoutInflater inflater = LayoutInflater.from(this);
        RelativeLayout layout = (RelativeLayout) inflater.inflate(
            R.layout.splash_screen, null, false);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {

            @Override
            public void run()
            {
                startActivity(new Intent(ActivitySplashScreen.this, LauncherActivity.class));
            }

        }, SPLASH_MILLIS);
    }

    public void launchTextReco(View v) {
        startActivity(new Intent(ActivitySplashScreen.this, TextRecognitionActivity.class));
    }

    public void launchPictoReco(View v) {
        startActivity(new Intent(ActivitySplashScreen.this, RecognitionActivity.class));
    }
}

